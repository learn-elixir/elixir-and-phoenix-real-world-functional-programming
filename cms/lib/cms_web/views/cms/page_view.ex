defmodule CmsWeb.CMS.PageView do
  use CmsWeb, :view
  alias Cms.CMS

  def author_name(%CMS.Page{author: author}) do
    author.user.name
  end
end
