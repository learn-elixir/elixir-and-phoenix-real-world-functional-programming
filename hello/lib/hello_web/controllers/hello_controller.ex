defmodule HelloWeb.HelloController do
  use HelloWeb, :controller

  def index(conn, _params) do
    conn
    # |> put_resp_content_type("text/plain") # will render page as plain text
    |> assign(:var, "I am a String")
    |> put_flash(:info, "Welcome back!")
    |> render("index.html")
  end

  def show(conn, %{"person" => person}) do
    render conn, "show.html", person: person
  end
end
